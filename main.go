package main

import (
	"encoding/json"
	"log"
	"net/http"
)

type Ketan struct {
	Menu string
	Tipe []string
}

type Health struct {
	Status string
}

func Index(w http.ResponseWriter, r *http.Request) {
	k := Ketan{"ketan", []string{"bakar original", "bakar keju"}}
	resp, err := json.Marshal(k)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(resp)
}

func HealthCheck(w http.ResponseWriter, r *http.Request) {
	s := Health{"good"}
	resp, err := json.Marshal(s)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")

	w.Write(resp)
}

func main() {
	http.HandleFunc("/health", HealthCheck)
	http.HandleFunc("/", Index)

	log.Println("ketan running at localhost:8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
